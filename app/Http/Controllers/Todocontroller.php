<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;


class Todocontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    //$book= Book::all();
    //return view('books.index',['books' => $book]);
    $id = Auth::id();
    if (Gate::denies('manager')) {
        $boss = DB::table('employees')->where('employee',$id)->first();
        $id = $boss->manager;
    }
    $books = User::find($id)->books;
    return view('books.index', compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create todos..");
        }
 
        return view('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);
       
    
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }
        $book = new Book();
        $id =Auth::id();
        $book->title = $request->title;
        $book->author = $request->author;
        $book->status = 0;
        $book->user_id = $id;
        $book->save();
        return redirect('books');
    
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     
        $book = Book::find($id);
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}
        return view('books.edit', compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = Book::find($id);
        if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit todos..");
        }   
    
 
        if(!$book->user->id == Auth::id())return (redirect('books'));
        
        $book->update($request->except(['_token']));
        if ($request->ajax()){
            return response::json(array('result'=>'success','status'=>$request->status),200);
        }
        return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");}
        if(!$book->user->id == Auth::id())return (redirect('books'));
        $book->delete();
        return redirect('books');
    }
}
