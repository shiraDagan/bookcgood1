install.packages('ggplot2')
library(ggplot2)

install.packages("ggplot2movies")
x<-library(ggplot2movies)
str(x)
colnames(movies)
nrow(movies)
#Q1
pl<-ggplot(movies,aes(rating))
pl+geom_histogram()
pl+geom_histogram(binwidth=.1)+xlab('Movie Rating')+ylab('Frequency')+
  ggtitle("Distribution of User\'s Rating")
#������� ������� �������
#Q2
ply<-ggplot(movies,aes(factor(year),rating))
ply+geom_boxplot()
ggplot(movies, aes(x=factor(Action), y=rating)) +
  stat_summary(fun.y="mean", geom="bar")
ply<-ggplot(movies,aes(budget,rating))
ply+geom_point()+stat_smooth()
